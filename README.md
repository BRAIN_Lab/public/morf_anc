# <center> Adaptive Neuroendocrine Control (ANC)</center>


## Introduction

This project provides the adaptive neuroendocrine control (ANC), consisting of three modules: Neural CPG-based Control (NC) to generate the basic walking pattern for the insect-like gait locomotion, Artificial Hormone Network (AHN) for terrain adaptation, and Input Correlation-based Learning (ICO) for obstacle negotiation. This control mechanism is developed based on the hexapod robot named Modular Robot Framework (MORF). The system allows the robot to perform the locomotion through the uneven complex terrains and unpredictable situation with energy-efficiency.

## Framework

The project is organized by three sub-folders including **controllers**, **projects**, and **utils**. 

- **controllers** contains the code of the ANC methods where the artificial hormone network is in:
	- controllers/neuroendocrine_control/real/AritificialHormoneNetwork.cpp
	- controllers/neuroendocrine_control/real/ArtiificialHormoneNetwork.h
	the neural CPG-based control is in:
	- controllers/neuroendocrine_control/real/NeuralControl.cpp
	- controllers/neuroendocrine_control/real/NeuralControl.h
	the input correlation-based learning and the integrating program are in:
	- controllers/neuroendocrine_control/real/neutronController.cpp
	- controllers/neuroendocrine_control/real/neutronController.h
	which the input correlation-based learning section is commented (line 289 - 393).
- **projects** contains the catkin workspace which is used for compile the program to get the executed file. to run the robot. The executed file is in: 
	- projects/neuroendocrine_control/real/catkin_ws/src/morf_controller/bin/morf_controller_real
- **utils** contains the framework library file. In this work we has the hormone framework named hormone_general which is in:
	- utils/hormone-framework/hormone_general.cpp
	- utils/hormone-framework/hormone_general.h 

## The implementation of the project
### Software Installation (Support OS: Ubuntu 18.04).
The system requires ROS melodic and Dynamixel SDK and ROS Controller which can be installed via command line. This should be done in both your computer and the robot's computer. In this work we use NUC as our computer.
The installation process and the robot's computer setup can be viewed at: https://github.com/MathiasThor/my_dynamixel_workbench/wiki/MORF-Software-Installation-Guide

### Steps to compile and run the robot
- open a terminal and go to the catkin_ws folder to run the command: catkin build **Note that** Only the first time before running catkin build, you need to run catkin clean first.
- go to the folder that contains the executed file and transfer the file to the robot's computer platform. 
- open a terminal in the robot's computer and run this roslauch command: roslaunch my_dynamixel_workbench_tutorial multiple_motor_test.launch
- open a new terminal in the robot's computer and go to the folder that contains the executed file and run: ./morf_controller_real

There is a tool named ansible which can help you run the process easier which you can follow in: https://github.com/MathiasThor/my_dynamixel_workbench/wiki/How-to-control-and-program-MORF




If you have any questions/doubts, you are welcomed to email to me. My email address is jettanan_hom@hotmail.com.
