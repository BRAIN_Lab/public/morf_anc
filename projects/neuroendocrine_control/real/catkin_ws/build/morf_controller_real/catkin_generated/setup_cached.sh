#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/jettanan/morf_ANC/projects/neuroendocrine_control/real/catkin_ws/devel/.private/morf_controller_real:$CMAKE_PREFIX_PATH"
export PWD="/home/jettanan/morf_ANC/projects/neuroendocrine_control/real/catkin_ws/build/morf_controller_real"
export ROSLISP_PACKAGE_DIRECTORIES="/home/jettanan/morf_ANC/projects/neuroendocrine_control/real/catkin_ws/devel/.private/morf_controller_real/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/jettanan/morf_ANC/projects/neuroendocrine_control/real/catkin_ws/src/morf_controller:$ROS_PACKAGE_PATH"