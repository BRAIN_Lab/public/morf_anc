/*
 * Written by Mathias Thor DEC 30
 * Happy NewYear!
 */

#include "neutronController.h"

// Main code:
int main(int argc,char* argv[])
{
    neutronController controller(argc,argv);
    while(controller.runController()){}
    return(0);
}
