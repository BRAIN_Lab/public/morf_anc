//
// Created by mat on 8/2/17.
//

#ifndef ROS_HEXAPOD_CONTROLLER_realROSCLASS_H
#define ROS_HEXAPOD_CONTROLLER_realROSCLASS_H

#include <cstdio>
#include <cstdlib>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include "sensor_msgs/Imu.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Float32MultiArray.h"
#include "std_msgs/Int32MultiArray.h"
//#include <dynamixel_workbench_msgs/JointCommand.h>
#include <rosgraph_msgs/Clock.h>
#include <sensor_msgs/JointState.h>

class realRosClass {
private:
    // Subscribers
    ros::Subscriber motorFeedbackSub;
    ros::Subscriber joint_IDs;
    ros::Subscriber joint_positions;
    ros::Subscriber joint_torques;
    ros::Subscriber joint_velocities;
    ros::Subscriber joint_errorStates;
    ros::Subscriber imu_9dof;
    ros::Subscriber current_sensor;

    // Publishers
    ros::Publisher jointControlPub;
    ros::Publisher variableValuePub;

    // Private Global Variables
    ros::Rate* rate;

    // Private Methods
    void joint_IDs_CB(const std_msgs::Int32MultiArray&);
    void joint_positions_CB(const std_msgs::Float32MultiArray&);
    void joint_torques_CB(const std_msgs::Float32MultiArray&);
    void joint_velocities_CB(const std_msgs::Float32MultiArray&);
    void joint_errorStates_CB(const std_msgs::Float32MultiArray&);
    void imu_9dof_CB(const sensor_msgs::Imu);
    void current_sensor_CB(const std_msgs::Float32);


public:
    // Public Methods
    realRosClass(int argc, char *argv[]);
    ~realRosClass();
    void setLegMotorPosition(std::vector<float> positions);
    void variableValue_Publish(std::vector<float> variables);
    void rosSpinOnce();

    // Public Global Variables
    std::vector<int> jointIDs = {0,0,0};
    std::vector<float> jointPositions = {0,0,0};
    std::vector<float> jointVelocities = {0,0,0};
    std::vector<float> jointTorques = {0,0,0};
    std::vector<float> jointErrorStates = {0,0,0};
    float current = 0;
    std::vector<float> Orientation = {0,0,0,0};
    std::vector<float> Angular_Velocity = {0,0,0};
    std::vector<float> Linear_Acceleration = {0,0,0};


};


#endif //ROS_HEXAPOD_CONTROLLER_realROSCLASS_H
