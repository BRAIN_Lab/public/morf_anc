//
// Created by mat on 12/30/17.
//

#include "NeuralControl.h"

NeuralControl::NeuralControl() {

	m_pre.resize(18);
    count_up.resize(2);
    count_down.resize(2);
    count_upold.resize(2);
    count_downold.resize(2);
    x_up.resize(2);
    x_down.resize(2);
    y_up.resize(2);
    y_down.resize(2);
    set.resize(2);
    delta_xup.resize(2);
    delta_xdown.resize(2);
    pcpg_output.resize(2);
    diff_set.resize(2);
    acc_error.resize(6);
    cpg_w.resize(2);
    cpg_w.at(0).resize(2);
    cpg_w.at(1).resize(2);
    cpg_activity.resize(2);
    cpg_output.resize(2);
    Control_input.resize(1);
    Control_input.at(0) = 0.01;
    cpg_output.at(0) = 0.1;
    cpg_output.at(1) = 0.1;

    for(int i=0;i<3;i++){
            buffer_motor[i].resize(300) ;
    }
}
NeuralControl::~NeuralControl(){

}
void NeuralControl::step() {
	tau=25;
    double set_old[2] = {set.at(0),set.at(1)};
    double old_pcpg_output[2] = {pcpg_output.at(0),pcpg_output.at(1)}	;
    //***Neural Control***//start
    cpg_w.at(0).at(0) =  1.4;
    cpg_w.at(0).at(1) =  0.18+Control_input.at(0);//0.4;
    cpg_w.at(1).at(0) =  -0.18-Control_input.at(0);//-0.4
    cpg_w.at(1).at(1) =  1.4;
    cpg_bias = 0;
    cpg_activity.at(0) = cpg_w.at(0).at(0) * cpg_output.at(0) + cpg_w.at(0).at(1) * cpg_output.at(1) + cpg_bias;
    cpg_activity.at(1) = cpg_w.at(1).at(1) * cpg_output.at(1) + cpg_w.at(1).at(0) * cpg_output.at(0) + cpg_bias;

    for(unsigned int i=0; i<2;i++)
    {
      cpg_output.at(i) = tanh(cpg_activity.at(i));
    }
    float MotorOutput1 = cpg_output.at(0);
    float MotorOutput4 = cpg_output.at(1);
    float MotorOutput2 = 0 ;
    //if(joySpeed != 0 && useJoy)
    {
    count_upold.at(0) = count_up.at(0);
    count_upold.at(1) = count_up.at(1);

    count_downold.at(0) = count_down.at(0);
    count_downold.at(1) = count_down.at(1);


    if(MotorOutput1>0.86){
	set.at(0) = 1;
    }
    else if(MotorOutput1<=0.86){
	set.at(0) = -1;
    }
    if(MotorOutput4>0.86){
	set.at(1) = 1;
    }
    else if(MotorOutput4<=0.86){
	set.at(1) = -1;
    }

    diff_set.at(0) = set.at(0) - set_old[0];
    diff_set.at(1) = set.at(1) - set_old[1];

    if(set.at(0) == 1.0)
    {
       count_up.at(0) = count_up.at(0)+1.0;
       count_down.at(0) = 0.0;
    }
    else if(set.at(0) == -1.0)
    {
      count_down.at(0) = count_down.at(0)+1.0;
      count_up.at(0) = 0.0;
    }
    if(set.at(1) == 1.0)
    {
      count_up.at(1) = count_up.at(1)+1.0;
      count_down.at(1) = 0.0;
    }
    else if (set.at(1) == -1.0)
    {
      count_down.at(1) = count_down.at(1)+1.0;
      count_up.at(1) = 0.0;
    }

    if(count_up.at(0) == 0.0 && diff_set.at(0) == -2.0 && set.at(0) == -1.0)
      delta_xup.at(0) = count_upold.at(0);

    if(count_down.at(0) == 0.0 && diff_set.at(0) == 2.0 && set.at(0) == 1.0)
      delta_xdown.at(0) = count_downold.at(0);

    if(count_up.at(1) == 0.0 && diff_set.at(1) == -2.0 && set.at(1) == -1.0)
      delta_xup.at(1) = count_upold.at(1);

    if(count_down.at(1) == 0.0 && diff_set.at(1) == 2.0 && set.at(1) == 1.0)
      delta_xdown.at(1) = count_downold.at(1);

    if (count_up.at(0) == 0.0 && diff_set.at(0) == -2.0 && set.at(0) == -1.0)
      delta_xup.at(0) = count_upold.at(0);

    if (count_down.at(0) == 0.0 && diff_set.at(0) == 2.0 && set.at(0) == 1.0)
      delta_xdown.at(0) = count_downold.at(0);

    if (count_up.at(1) == 0.0 && diff_set.at(1) == -2.0 && set.at(1) == -1.0)
      delta_xup.at(1) = count_upold.at(1);

    if (count_down.at(1) == 0.0 && diff_set.at(1) == 2.0 && set.at(1) == 1.0)
      delta_xdown.at(1) = count_downold.at(1);

    x_up.at(0) =  count_up.at(0);
    x_down.at(0) = count_down.at(0);

    x_up.at(1) =  count_up.at(1);
    x_down.at(1) = count_down.at(1);

    y_up.at(0) = ((2./delta_xup.at(0))*x_up.at(0))-1;
    y_down.at(0) = ((-2./delta_xdown.at(0))*x_down.at(0))+1;

    y_up.at(1) = ((2./delta_xup.at(1))*x_up.at(1))-1;
    y_down.at(1) = ((-2./delta_xdown.at(1))*x_down.at(1))+1;

    if (set.at(0) >= 0.0)
      pcpg_output.at(0) = y_up.at(0);

    if (set.at(0) < 0.0)
      pcpg_output.at(0) = y_down.at(0);

    if (set.at(1) >= 0.0)
      pcpg_output.at(1) = y_up.at(1);

    if (set.at(1) < 0.0)
      pcpg_output.at(1) = y_down.at(1);

    if(pcpg_output.at(0)>1.0)
      pcpg_output.at(0) = 1.0;
    else if(pcpg_output.at(0)<-1.0)
      pcpg_output.at(0) = -1.0;

    if(pcpg_output.at(1)>1.0)
      pcpg_output.at(1) = 1.0;

    else if(pcpg_output.at(1)<-1.0)
      pcpg_output.at(1) = -1.0;

    pcpg_output.at(0) = (0.3)*old_pcpg_output[0] + (1-0.3)*pcpg_output.at(0);
    pcpg_output.at(1) = (0.3)*old_pcpg_output[1] + (1-0.3)*pcpg_output.at(1);
    for(int i=0;i<3;i++){
      if (buffer_motor[i].size() >= 300) {
         buffer_motor[i].erase(buffer_motor[i].begin());
      }
    }
    buffer_motor[0].push_back(pcpg_output.at(0));

    //cout << pcpg_output.at(0) << " " << buffer_motor[0].at(122-2) << " " << buffer_motor[0].at(122-1) << " " << buffer_motor[0].size()  << endl;
    if((old_pcpg_output[0]-pcpg_output.at(0)<0)){
	buffer_motor[1].push_back(pcpg_output.at(0));
	count = 0;
    }
    else{
	buffer_motor[1].push_back(-1.0);
    }

    if(count!=10 && !(old_pcpg_output[0]-pcpg_output.at(0)<0)){
    	count++;
    	//cout << buffer_motor[1].size();
    	buffer_motor[1].at(299) = buffer_motor[1].at(299)*0.1 + buffer_motor[1].at(298)*0.9;
    }
	int offset = 5;
	int offset2 = 1;
   	m_pre.at(0) = buffer_motor[0].at(300-offset-(tau*5));
	m_pre.at(1) = buffer_motor[0].at(300-offset-(tau*4));
	m_pre.at(2) = buffer_motor[0].at(300-offset-(tau*3));
	m_pre.at(3) = buffer_motor[0].at(300-offset-(tau*2));
	m_pre.at(4) = buffer_motor[0].at(300-offset-(tau*1));
	m_pre.at(5) = buffer_motor[0].at(300-offset-(tau*0));

	m_pre.at(6) = buffer_motor[1].at(300-offset2-(tau*5));
	m_pre.at(7) = buffer_motor[1].at(300-offset2-(tau*4));
	m_pre.at(8) = buffer_motor[1].at(300-offset2-(tau*3));
	m_pre.at(9) = buffer_motor[1].at(300-offset2-(tau*2));
	m_pre.at(10) = buffer_motor[1].at(300-offset2-(tau*1));
	m_pre.at(11) = buffer_motor[1].at(300-offset2-(tau*0));

	m_pre.at(12) = buffer_motor[1].at(300-offset2-(tau*5));
	m_pre.at(13) = buffer_motor[1].at(300-offset2-(tau*4));
	m_pre.at(14) = buffer_motor[1].at(300-offset2-(tau*3));
	m_pre.at(15) = buffer_motor[1].at(300-offset2-(tau*2));
	m_pre.at(16) = buffer_motor[1].at(300-offset2-(tau*1));
	m_pre.at(17) = buffer_motor[1].at(300-offset2-(tau*0));
	for(int i=0;i<18;i++){
		m_pre.at(i) = (double)tanh(m_pre.at(i)*2.0);
	}
    }
}
