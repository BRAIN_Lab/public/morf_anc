#ifndef _ARTIFICIAL_HORMONE_NETWORK_
#define  _ARTIFICIAL_HORMONE_NETWORK_


#include "utils/hormone-framework/hormone_general.h"
#include <algorithm>
#include <vector>
#include <cmath>

//Save files
#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;
using namespace Hormone;

class ArtificialHormoneNetwork {
  private:
    //Method initial
    void createAHN();
    void createGland(unsigned int _size);
    void createHormoneTank(unsigned int _size);
    void createReceptor(unsigned int _size);

    void hormoneConfig();
    void configGlandAlpha(unsigned int _Address, float _Alpha);
    void configGlandType(unsigned int _Address, int _Type, int _Recordtype);
    void configGlandBufferSize(unsigned int _Address, unsigned int _Buffersize);
    void configTankBeta(unsigned int _Address, float _Beta);

    void configReceptorRate(unsigned int _Address, unsigned int _Destination, DATATYPE rate);
    void configReceptorWeight(unsigned int _Address, unsigned int _Destination, DATATYPE weight);
    void configGlandMode(unsigned int _Address, unsigned int _Destination, int);
    void configGlandParam(unsigned int _Address, unsigned int _Destination, DATATYPE);
    void configGlandWeight(unsigned int _Address, unsigned int _Destination, DATATYPE);

    void hormoneConnection();
    void connectGlandToTank(unsigned int _Address, unsigned int _Destination);
    void connectTankToGland(unsigned int _Address, unsigned int _Destination, int mode = 1, DATATYPE parameter = 0,
        DATATYPE weight = 1);
    void connectReceptorToTank(unsigned int _Address, unsigned int _Destination, DATATYPE rate = 1,
        DATATYPE Weight = 1);

    //Method loop
    //preprocess outter can't edit but inheritance must edit
    void preprocess();
    void updateDataGland();
    void updateHormone();

    //variable
    unsigned int gland_size;
    unsigned int hormone_size;
    unsigned int receptor_size;

    unsigned int windows_size;
    unsigned int internal_size;

    std::vector<gland> HGland;
    std::vector<hormone> HTank;
    std::vector<receptor> HReceptor;

    //data input from sensors and motors
    std::vector<std::vector<double>> in0;
    std::vector<std::vector<double>> out0;
    std::vector<std::vector<double>> inside0;

    std::vector<std::vector<double>> data_sensor;
    std::vector<std::vector<double>> data_motor;
    std::vector<std::vector<double>> data_inside;

    //method clear and resize
    void clearData(vector<double> &x, unsigned int _size, DATATYPE = 0.0);
    void clearData(vector<vector<double>> &x, unsigned int _size, unsigned int _windows, DATATYPE = 0.0);

    //math method for preprocess
    DATATYPE Convolution(const vector<double> x, const vector<double> y);
    DATATYPE Correlation(const vector<double> x, const vector<double> y);
    DATATYPE calcSD(vector<double> inputVector);
    DATATYPE calcMean(vector<double> inputVector);
    DATATYPE calcCov(const vector<double> x, const vector<double> y);

    void birdModel(double weight, double scaling[6]);
    void gearModel(double weight, double scaling);
    //---Start Save files---//
    ofstream outFileAHN;
    ofstream outFileBB;
    //---End Save files---//

  public:
    ArtificialHormoneNetwork();
    ~ArtificialHormoneNetwork();

    void initialAHN();
    void hormoneProcess();
    void reAHN();

    void externalStimulus(const vector<double> x, const vector<double> y);
    void internalStimulus(const vector<double> x);

    //hormone Active
    DATATYPE receptorActive(unsigned int);

    //report
    DATATYPE getHormoneConcentration(unsigned int);
    DATATYPE getDataInside(unsigned int);

    //configuration
    void setGlandSize(unsigned int);
    unsigned int getGlandSize();
    void setHormoneSize(unsigned int);
    unsigned int getHormoneSize();
    void setReceptorSize(unsigned int);
    unsigned int getReceptorSize();

    void setWindowsSize(unsigned int);
    unsigned int getWindowsSize();

    void setInternalSize(unsigned int);
    unsigned int getInternalSize();

};

#endif
