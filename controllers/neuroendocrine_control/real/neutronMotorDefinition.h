//
// Created by mat on 8/26/17.
//

#ifndef NEUTRON_CONTROLLER_NEURONMOTORDEFINITION_H
#define NEUTRON_CONTROLLER_NEURONMOTORDEFINITION_H

enum NeutronSensorNames{
    NEURON_SENSORAX = 0,
};

enum NeutronMotorNames{
    BC0 = 0,
    BC1 = 1,
    BC2 = 2, // Upward (+), Downward (-)
    BC3 = 3,
    BC4 = 4,
    BC5 = 5,

    CF0 = 6,
    CF1 = 7,
    CF2 = 8, // Upward (+), Downward (-)
    CF3 = 9,
    CF4 = 10,
    CF5 = 11,

    FT0 = 12,
    FT1 = 13,
    FT2 = 14, // Upward (+), Downward (-)
    FT3 = 15,
    FT4 = 16,
    FT5 = 17,

    //Changing according to the maximum motor number
    NEURON_MOTOR_MAX = 18,

	BC0_jp = 0,
    BC1_jp = 1,
    BC2_jp = 2, // Upward (+), Downward (-)
    BC3_jp = 3,
    BC4_jp = 4,
    BC5_jp = 5,

    CF0_jp = 6,
    CF1_jp = 7,
    CF2_jp = 8, // Upward (+), Downward (-)
    CF3_jp = 9,
    CF4_jp = 10,
    CF5_jp = 11,

    FT0_jp = 12,
    FT1_jp = 13,
    FT2_jp = 14, // Upward (+), Downward (-)
    FT3_jp = 15,
    FT4_jp = 16,
    FT5_jp = 17,

    BC0_ts = 18,
    BC1_ts = 19,
    BC2_ts = 20, // Upward (+), Downward (-)
    BC3_ts = 21,
    BC4_ts = 22,
    BC5_ts = 23,

    CF0_ts = 24,
    CF1_ts = 25,
    CF2_ts = 26, // Upward (+), Downward (-)
    CF3_ts = 27,
    CF4_ts = 28,
    CF5_ts = 29,

    FT0_ts = 30,
    FT1_ts = 31,
    FT2_ts = 32, // Upward (+), Downward (-)
    FT3_ts = 33,
    FT4_ts = 34,
    FT5_ts = 35,

	Or_x = 36,
	Or_y = 37,
	Or_z = 38,
	Or_w = 39,

	AnVel_x = 40,
	AnVel_y = 41,
	AnVel_z = 42,

	Acc_x = 43,
	Acc_y = 44,
	Acc_z = 45,

	ACS = 46,


};

#endif //NEUTRON_CONTROLLER_NEURONMOTORDEFINITION_H
