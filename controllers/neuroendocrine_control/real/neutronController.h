//
// Created by mat on 12/30/17.
//

#ifndef NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H
#define NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <algorithm>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "realRosClass.h"
#include "NeuralControl.h"
#include "neutronMotorDefinition.h"
#include "ArtificialHormoneNetwork.h"

class NeuralControl;

class neutronController {
public:
    neutronController(int argc,char* argv[]);
    ~neutronController();
    bool runController();
private:
    double rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter);
    void actuateRobot();
    void Hormone();
    void Reflex();
    void sensor_preprocess();

    //ofstream myfile;
    ofstream external_sensors;
    ofstream ico_learning;
    ofstream p_gear;

    vector<double> m_pre_p;
    vector<double> motor_signal[18];
    double back=1.0;
    int global_count=0;
    int inner_count[6]={0,0,0,0,0,0};
    double offset[6]={0,0,0,0,0,0};
    vector<double> sensors;
    vector<double> acc_error;
    bool reflex_active=true;
    bool distributed_hormone_active=false;
    bool global_hormone_active=false;

    vector<double> positions;
    std::vector<float> data;
    std::vector<double> lift_value;    

    double climb = 1;
	double receptor[9]={0,0,0,0,0,0,0,0,0} ;
    double mem[6] = {0,0,0,0,0,0};
    double mem_ft[6] ={0,0,0,0,0,0};
    double mem_value[6]={0,0,0,0,0,0};
    std::vector<double> reflexive_ico[6];
    std::vector<double> reflexive_update[6];
    std::vector<double> predictive_ico[6];
    std::vector<double> sensors_p;
    double w_0[6] ={1,1,1,1,1,1};
    double w_1[6] ={0,-260,-260,0,-260,-260};//{0,-150.758,-236.085,0,-151.926,0};
    //{0,-260,-400,0,-269.616,-406.09}; //for MI = 0.01
    double dw_1[6] ={0,0,0,0,0,0};
    double output_ico[6];
    int ack[6]={0,0,0,0,0,0};
    double mu[6] ={-20,-30,-50,-20,-30,-50};
    double st_control[6]={0,0,0,0,0,0};
    double FT_offset[6]={0,0,0,0,0,0};
    int count_offset[6]={0,0,0,0,0,0};
    int count_mem[6]={0,0,0,0,0,0};
    int count_print=0;
    double activated[6]={0,0,0,0,0,0};
    int count_all=0;
    /* 0. Vanilla CPG SO2
     * 1. Dual Learner
     * 2. Adaptive frequency
     * 3. DL + AF -> NN (Hard Limiter and SigmoidII) */

    realRosClass * realRos;
    ArtificialHormoneNetwork* hormone_controller; // Adding Artificial Hormone
    NeuralControl* neural_control;
};


#endif //NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H
