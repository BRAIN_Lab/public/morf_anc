//
// Created by mat on 8/2/17.
//

#include "realRosClass.h"

realRosClass::realRosClass(int argc, char **argv) {
    // Create a ROS nodes
    int _argc = 0;
    char** _argv = NULL;
    ros::init(_argc,_argv,"MORF_controller");

    if(!ros::master::check())
        ROS_ERROR("ros::master::check() did not pass!");

    ros::NodeHandle node("~");
    ROS_INFO("realROS just started!");

    // Initialize Subscribers
    //motorFeedbackSub=node.subscribe("/joint_states", 1, &realRosClass::motorFeedbackCallback, this);
    joint_IDs=node.subscribe("/joint_IDs", 1, &realRosClass::joint_IDs_CB, this);;
    joint_positions=node.subscribe("/joint_positions", 1, &realRosClass::joint_positions_CB, this);;
    joint_torques=node.subscribe("/joint_torques", 1, &realRosClass::joint_torques_CB, this);;
    joint_velocities=node.subscribe("/joint_velocities", 1, &realRosClass::joint_velocities_CB, this);;
    joint_errorStates=node.subscribe("/joint_errorStates", 1, &realRosClass::joint_errorStates_CB, this);;
    imu_9dof=node.subscribe("/imu", 1, &realRosClass::imu_9dof_CB, this);;
    current_sensor=node.subscribe("/current", 1, &realRosClass::current_sensor_CB, this);;

    // Initialize Publishers
    jointControlPub=node.advertise<std_msgs::Float32MultiArray>("/multi_joint_command",1);
    variableValuePub=node.advertise<std_msgs::Float32MultiArray>("/variable_value",1);

    // Set Rate
    rate = new ros::Rate(30); // 60hz
}

void realRosClass::joint_IDs_CB(const std_msgs::Int32MultiArray& _jointIDs) {
    jointIDs = _jointIDs.data;
}

void realRosClass::joint_positions_CB(const std_msgs::Float32MultiArray& _jointPositions) {
    jointPositions = _jointPositions.data;
    
}

void realRosClass::joint_torques_CB(const std_msgs::Float32MultiArray& _jointTorques) {
    jointTorques = _jointTorques.data;
}

void realRosClass::joint_velocities_CB(const std_msgs::Float32MultiArray& _jointVelocities) {
    jointVelocities = _jointVelocities.data;
}

void realRosClass::joint_errorStates_CB(const std_msgs::Float32MultiArray& _jointErrorStates) {
    jointErrorStates = _jointErrorStates.data;
}
void realRosClass::imu_9dof_CB(const sensor_msgs::Imu _imu_9dof) {
    Orientation.at(0) = _imu_9dof.orientation.x ;
    Orientation.at(1) = _imu_9dof.orientation.y ;
    Orientation.at(2) = _imu_9dof.orientation.z ;
    Orientation.at(3) = _imu_9dof.orientation.w ;

    Angular_Velocity.at(0) = _imu_9dof.angular_velocity.x ;
    Angular_Velocity.at(1) = _imu_9dof.angular_velocity.y ;
    Angular_Velocity.at(2) = _imu_9dof.angular_velocity.z ;

    Linear_Acceleration.at(0) = _imu_9dof.linear_acceleration.x ;
    Linear_Acceleration.at(1) = _imu_9dof.linear_acceleration.y ;
    Linear_Acceleration.at(2) = _imu_9dof.linear_acceleration.z ;
}
void realRosClass::current_sensor_CB(const std_msgs::Float32 _current_sensor) {
    current = _current_sensor.data;
}

void realRosClass::setLegMotorPosition(std::vector<float> positions) {
    // publish the motor positions:
    std_msgs::Float32MultiArray array;
    array.data.clear();

    for (float position : positions)
        array.data.push_back(position);

    jointControlPub.publish(array);
}
void realRosClass::variableValue_Publish(std::vector<float> variables) {
    // publish the motor positions:
    std_msgs::Float32MultiArray array;
    array.data.clear();

    for (float variable : variables)
        array.data.push_back(variable);

    variableValuePub.publish(array);
}
void realRosClass::rosSpinOnce(){
    ros::spinOnce();
    bool rateMet = rate->sleep();

    if(!rateMet)
    {
        ROS_ERROR("Sleep rate not met");
    }

}

realRosClass::~realRosClass() {
    ROS_INFO("realROS just terminated!");
    ros::shutdown();
}
