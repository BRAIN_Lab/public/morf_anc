//
// Created by mat on 12/30/17.
//

#include "neutronController.h"
#include <chrono>

using namespace std::chrono ;

neutronController::neutronController(int argc,char* argv[]) {
	char mode;
	cout << "select mode : ";
	cin >> mode;
	if(mode=='r'){
	    reflex_active=true;
	    distributed_hormone_active=false;
	}
	else if(mode=='h'){
	    reflex_active=false;
	    distributed_hormone_active=true;
	}
	m_pre_p.resize(18);
    sensors.resize(122);
    sensors_p.resize(122);
    acc_error.resize(6);
    lift_value.resize(6);
    for(int i=0;i<lift_value.size();i++){
		lift_value.at(i) = 1.2;
    }
    for(int i=0;i<6;i++){
		predictive_ico[i].resize(500);
		reflexive_ico[i].resize(2);
		reflexive_update[i].resize(2);
    }
    for(int i=0;i<18;i++){
    	motor_signal[i].resize(300);
    }
    for(int i=0;i<sensors.size();i++){
    	sensors.at(i) = 0;
    	sensors_p.at(i) = 0;
    }

	hormone_controller = new ArtificialHormoneNetwork();
  	hormone_controller->initialAHN();
    realRos = new realRosClass(argc, argv);

    neural_control = new NeuralControl();

    positions.resize(19);

    external_sensors.open("external_sensors.txt");
    external_sensors << "Or_x Or_y Or_z Or_w AnVel_x AnVel_y AnVel_z Acc_x Acc_y Acc_z ACS MI" << endl;
    ico_learning.open("ico_learning.txt");
    for(int i=0;i<6;i++){
    	ico_learning << "w0[" << i << "] " << "w1[" << i << "] " << "dw[" << i << "] " << "reflexive_ico[" << i << "] reflexive_update[" << i << "] predictive[" << i << "] output[" << i << "] ";
    }
    ico_learning << endl;
}
neutronController::~neutronController(){
	external_sensors.close();
}
bool neutronController::runController() {
    if(ros::ok()) {
    	high_resolution_clock::time_point t1 = high_resolution_clock::now();
    	neural_control->step();
        actuateRobot();
        high_resolution_clock::time_point t2 = high_resolution_clock::now();
        duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
        realRos->rosSpinOnce();

        return true;

    } else
    {
        cout << "Shutting Down out of da loop" << endl;
        return false;
    }
}

double neutronController::rescale(double oldMax, double oldMin, double newMax, double newMin, double parameter){
    return (((newMax-newMin)*(parameter-oldMin))/(oldMax-oldMin))+newMin;
}
static void toEulerAngle(double q[4], double& roll, double& pitch, double& yaw)
{
	// roll (x-axis rotation)
	double sinr_cosp = +2.0 * (q[3] * q[0] + q[1] * q[2]);
	double cosr_cosp = +1.0 - 2.0 * (q[0] * q[0] + q[1] * q[1]);
	roll = atan2(sinr_cosp, cosr_cosp);

	// pitch (y-axis rotation)
	double sinp = +2.0 * (q[3] * q[1] - q[2] * q[0]);
	if (fabs(sinp) >= 1)
		pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
	else
		pitch = asin(sinp);

	// yaw (z-axis rotation)
	double siny_cosp = +2.0 * (q[3] * q[2] + q[0] * q[1]);
	double cosy_cosp = +1.0 - 2.0 * (q[1] * q[1] + q[2] * q[2]);
	yaw = atan2(siny_cosp, cosy_cosp);
}
void neutronController::Hormone() {
	vector<double> internal_data;
	internal_data.resize(36);
	for(int i=0;i<18;i++){
		internal_data.at(i) = neural_control->m_pre.at(i);
	}
	for(int i=18;i<24;i++){
		internal_data.at(i) = lift_value.at(i-18);
	}
	for(int i=24;i<30;i++){
		internal_data.at(i) = acc_error.at(i-24);
	}
	for(int i=30;i<36;i++){
		internal_data.at(i) = st_control[i-30];
	}
	if(global_count>300){
		hormone_controller->externalStimulus(sensors,positions); 		//insert joint command and all sensor into hormone program x = joint command , y = sensor value
		hormone_controller->internalStimulus(internal_data);		//insert another data which useful for the program
		hormone_controller->hormoneProcess(); 				//execute hormone process
  		ROS_INFO("**************************************");
  		for(int i=0;i<9;i++) {//edit this
  			//cout << receptor[i] << " ";
  			if(i<9)
  			ROS_INFO("receptor[%d] = %.3f", i,receptor[i]);
  			receptor[i]= hormone_controller->receptorActive(i);
		}

	}
}
void neutronController::Reflex() {
	for (int i = 0; i < 6; i++) {
      //low pass filter
	  double m_pre_p_p = m_pre_p.at(6+i);
	  double m_pre_rf = (((-neural_control->m_pre.at(6+i) + 1)/2) * 0.5 + m_pre_p.at(6+i) * (1-0.5))*0.2 ;//pre-process
	  m_pre_p.at(6+i) = m_pre_rf;
      inner_count[i]++;
      sensors.at(24+i) = exp(-sensors.at(24+i))/(1+exp(-sensors.at(24+i)));
      sensors.at(24+i) = sensors.at(24+i)-0.5;
      if(sensors.at(24+i)<0) sensors.at(24+i) = 0; //pre-process end
      if(m_pre_rf-m_pre_p_p < 0){sensors.at(24+i)=0; inner_count[i] = 0;}//0.964027
      double error = 0;



      if(global_count > 300) if(sensors.at(24+i)<0.05) error = m_pre_rf - sensors.at(24+i);
      //if(i==0 || i == 3) error *= 0.5;
      if(inner_count[i] > 10)
      acc_error.at(i)  += error/sqrt(1+pow(error,2))*0.3; //B-can edit
      if(m_pre_rf-m_pre_p_p < 0) {acc_error.at(i)=0.3;}//0.964027
    }
}
void neutronController::sensor_preprocess() {
	for(int i=BC0_jp;i<=FT5_jp;i++){
		sensors.at(i) = realRos->jointPositions[i];
		//cout << sensors.at(i);
	}
	//cout << endl ;
	sensors.at(BC0_ts) = realRos->jointTorques[0];
	sensors.at(BC1_ts) = realRos->jointTorques[3];
	sensors.at(BC2_ts) = realRos->jointTorques[6];
	sensors.at(BC3_ts) = -realRos->jointTorques[9];
	sensors.at(BC4_ts) = -realRos->jointTorques[12];
	sensors.at(BC5_ts) = -realRos->jointTorques[15];

	sensors.at(CF0_ts) = realRos->jointTorques[1];
	sensors.at(CF1_ts) = realRos->jointTorques[4];
	sensors.at(CF2_ts) = realRos->jointTorques[7];
	sensors.at(CF3_ts) = realRos->jointTorques[10];
	sensors.at(CF4_ts) = realRos->jointTorques[13];
	sensors.at(CF5_ts) = realRos->jointTorques[16];

	sensors.at(FT0_ts) = realRos->jointTorques[2];
	sensors.at(FT1_ts) = realRos->jointTorques[5];
	sensors.at(FT2_ts) = realRos->jointTorques[8];
	sensors.at(FT3_ts) = realRos->jointTorques[11];
	sensors.at(FT4_ts) = realRos->jointTorques[14];
	sensors.at(FT5_ts) = realRos->jointTorques[17];

	sensors.at(Or_x) = realRos->Orientation.at(0);
	sensors.at(Or_y) = realRos->Orientation.at(1);
	sensors.at(Or_z) = realRos->Orientation.at(2);
	sensors.at(Or_w) = realRos->Orientation.at(3);

	sensors.at(AnVel_x) = realRos->Angular_Velocity.at(0);
	sensors.at(AnVel_y) = realRos->Angular_Velocity.at(1);
	sensors.at(AnVel_z) = realRos->Angular_Velocity.at(2);

	sensors.at(Acc_x) = realRos->Linear_Acceleration.at(0);
	sensors.at(Acc_y) = realRos->Linear_Acceleration.at(1);
	sensors.at(Acc_z) = realRos->Linear_Acceleration.at(2);

	sensors.at(ACS) = realRos->current;
	double q[4] = {sensors.at(Or_x),sensors.at(Or_y),sensors.at(Or_z),sensors.at(Or_w)};
	toEulerAngle(q,sensors.at(Or_x),sensors.at(Or_y),sensors.at(Or_z));
	sensors.at(Or_x) = -(sensors.at(Or_x)+0.005);
	if(global_count>300){
	for(int i=Or_x;i<=ACS;i++){
		external_sensors << sensors.at(i) << " " ;
	}
	external_sensors << neural_control->Control_input.at(0) << " ";
	external_sensors << endl ;
	}
}
void neutronController::actuateRobot() {

    double turnFactorLEFT = 1.0;
    double turnFactorRIGHT = 1.0;
    double hfOffsetSignLEFT = 1.0;
    double hfOffsetSignRIGHT = 1.0;

    	sensor_preprocess();
	Reflex();
	Hormone();

	//check which system we want to use --> change in .h file
	if(reflex_active) {for(int i=0;i<6;i++) {lift_value.at(i) = acc_error.at(i) ;}}
	if(global_hormone_active) {for(int i=0;i<1;i++) {lift_value.at(i) = receptor[0];}}
	if(distributed_hormone_active) { double R7 = receptor[7]; double R8 = receptor[8];

		if(R7>1.5){ R8 -= (R7-1.5); R7 = 1.5;}
		if(R8>1.5){R7 -= (R8-1.5);R8 = 1.5;}

		if(neural_control->Control_input.at(0)>0.19){
			neural_control->Control_input.at(0)=0.19;
		}
		else if(neural_control->Control_input.at(0)<0.01){
			neural_control->Control_input.at(0)=0.01;
		}
		
		// assign the receptor value to leg offset named lift_value
		for(int i=0;i<6;i++){if(i<3)lift_value.at(i) = receptor[i]*R7; else	lift_value.at(i) = receptor[i]*R8;}
	}
	for(int i=0;i<lift_value.size();i++){
		if(lift_value.at(i)>1.5)
			lift_value.at(i) = 1.5 ;
		if(lift_value.at(i)<0)
			lift_value.at(i) = 0 ;
		lift_value.at(i) = 0.5;
    }

	positions.at(BC0) = (rescale(1,-1,0.3,-0.3,neural_control->m_pre.at(0)*back) * turnFactorLEFT)-0.1;
	positions.at(BC1) =  rescale(1,-1,0.3,-0.3,neural_control->m_pre.at(1)*back) * turnFactorLEFT *1.4-0.3;
	positions.at(BC2) = (rescale(1,-1,0.3,-0.3,neural_control->m_pre.at(2)*back) * turnFactorLEFT)*1-0.5;
	positions.at(BC3) = (rescale(1,-1,0.3,-0.3,neural_control->m_pre.at(3)*back) * turnFactorRIGHT)-0.1;
	positions.at(BC4) =  rescale(1,-1,0.3,-0.3,neural_control->m_pre.at(4)*back) * turnFactorRIGHT *1.4-0.3;
	positions.at(BC5) = (rescale(1,-1,0.3,-0.3,neural_control->m_pre.at(5)*back) * turnFactorRIGHT)*1-0.5;


    if(global_count <= 330) {global_count++;}
    for(int i=0;i<6;i++){
    	if(neural_control->m_pre.at(CF0+i)<=-0.964){
    		positions.at(CF0+i) = (rescale(1,0,3.1,2.8,rescale(1,-1,1,0,neural_control->m_pre.at(CF0+i)))-lift_value.at(i))*0.2 + positions.at(CF0+i)*0.8;//2.8
    	}
    	else{
		//here is if we want command the robot to swing its leg higher, change the condition of if to "true"
    		if(false) positions.at(CF0+i) = (rescale(1,0,3.1,2.8,rescale(1,-1,1,0,neural_control->m_pre.at(CF0+i))))*0.2 + positions.at(CF0+i)*0.8;//2.8
    		else positions.at(CF0+i) = (rescale(1,0,3.1,2.8,rescale(1,-1,1,0,neural_control->m_pre.at(CF0+i)))-lift_value.at(i))*0.2 + positions.at(CF0+i)*0.8;//2.8
    	}
    }

	for(int i=CF0; i <=CF5 ; i++){
		if(positions.at(i) < 1.2){
			positions.at(i) = 1.2 ;
			if(i==CF0)
			cout << "alert!!" << "\r\n";
		}
		if(positions.at(i) > 3.1){
			positions.at(i) = 3.1 ;
			if(i==CF0)
			cout << "alert!!" << "\r\n";
		}
	}	

    positions.at(FT0) = rescale(-1,1,0.2,-0.2,neural_control->m_pre.at(12))-lift_value.at(0);
    positions.at(FT1) = rescale(-1,1,0.2,-0.2,neural_control->m_pre.at(13))-lift_value.at(1);
    positions.at(FT2) = rescale(-1,1,0.2,-0.2,neural_control->m_pre.at(14))-lift_value.at(2);
    positions.at(FT3) = rescale(-1,1,0.2,-0.2,neural_control->m_pre.at(15))-lift_value.at(3);
    positions.at(FT4) = rescale(-1,1,0.2,-0.2,neural_control->m_pre.at(16))-lift_value.at(4);
    positions.at(FT5) = rescale(-1,1,0.2,-0.2,neural_control->m_pre.at(17))-lift_value.at(5);

    for(int i=0;i<18;i++){
    	motor_signal[i].push_back(positions.at(i));
		if (motor_signal[i].size() >= 300) {
			motor_signal[i].erase(motor_signal[i].begin());
		}
    }
    for(int i=0;i<6;i++){
    /****************************Obstacle Negotiation***********start**************/
    	if(st_control[i] ==1){
    		sensors.at(BC0_ts+i) = 0;
    	}
        if(motor_signal[BC0+i].at(299)-motor_signal[BC0+i].at(298)<0){
        	sensors.at(BC0_ts+i) = 0 ;

        }
    /** variable processing  start  **/
    if(global_count > 329){
    	double w[6] = {100,100,100,100,100,100};
    	double k[6] = {160,120,120,160,120,120};// for MI = 0.01
    	sensors.at(ACS+BC0_ts+i) = 1/(1+exp(-((-sensors.at(BC0_ts+i)*w[i])-k[i])));
    	sensors.at(ACS+(2*BC0_ts)+i) = sensors.at(ACS+BC0_ts+i);
    	if(!(sensors.at(ACS+BC0_ts+i)-sensors_p.at(ACS+BC0_ts+i)>0.3)) {
    		sensors.at(ACS+BC0_ts+i) = (0.01*sensors.at(ACS+BC0_ts+i)) + ((1-0.01)*sensors_p.at(ACS+BC0_ts+i));
    	}
    	if(sensors.at(ACS+BC0_ts+i)<0.3) sensors.at(ACS+BC0_ts+i)=0;
    }
    /** variable processing finished**/
    /** ICO Learning   start  **/
    reflexive_ico[i].push_back(sensors.at(ACS+(BC0_ts)+i));
    reflexive_update[i].push_back(sensors.at(ACS+(BC0_ts*2)+i));
    if(reflexive_ico[i].size() >2){
    	reflexive_ico[i].erase(reflexive_ico[i].begin());
    }
    if(reflexive_update[i].size() >2){
    	reflexive_update[i].erase(reflexive_update[i].begin());
    }
    output_ico[i] = w_0[i]*reflexive_ico[i].at(1);
    if(i!=0 && i!=3){
    	predictive_ico[i].push_back(output_ico[i-1]);
    	if(predictive_ico[i].size() >400){
    		predictive_ico[i].erase(predictive_ico[i].begin());
    	}
    	double reflexive_d = reflexive_update[i].at(1)-reflexive_update[i].at(0);
    	if(reflexive_d<0) reflexive_d = 0;

	
    	int index = 499+(int)w_1[i];
    	if(index>499) {index=499;}
    	if(index<0) {index=0;}
    	if(!reflex_active) {if(output_ico[i]<predictive_ico[i].at(index)) output_ico[i] = predictive_ico[i].at(index); }
    	if(i !=0 && i!=3) output_ico[i] = predictive_ico[i].at(index);
    	if(output_ico[i]>1) output_ico[i] = 1;
    	double x1 = predictive_ico[i].at(index);
    	if(x1==0){x1 = -0.3;}
    	else{x1 = 1-x1;}
    	dw_1[i] = mu[i]*(reflexive_d)*x1;
    }

    /** ICO Learning finished **/
    /** check for activating signal and assign the legs trajectory ::start::**/
    if(!reflex_active){
    if(output_ico[i]>0.9 && motor_signal[BC0+i].at(299)-motor_signal[BC0+i].at(298)<0){
    	ack[i] = 1;
    }}

    if(motor_signal[BC0+i].at(299)-motor_signal[BC0+i].at(298)>0){
    	if(st_control[0+i]==0){
    		count_offset[0+i]++;
    	}
    	if(ack[i]==1 || (output_ico[i]>0.9 && st_control[0+i] == 0)){ //output_ico[i]
    		ack[i] = 0;
    		positions.at(BC0+i) = positions.at(BC0+i);
    		FT_offset[0+i] = 1.0;
    		st_control[0+i] = 1;
    		count_mem[0+i] = count_offset[0+i];
    	}
    }
    else if(motor_signal[BC0+i].at(299)-motor_signal[BC0+i].at(298)<0 && st_control[0+i]==0){
    	FT_offset[0+i] = 0;
    	count_offset[0+i] = 0;
    }
    if(st_control[0+i]==1){
    	ack[i] = 0;
    	positions.at(BC0+i) = (motor_signal[BC0+i].at(299-count_offset[0+i])*1.5)+0.15;
    	positions.at(CF0+i) = motor_signal[CF0+i].at(299-count_offset[0+i])+lift_value.at(0+i);
    	positions.at(FT0+i) = motor_signal[FT0+i].at(299-count_offset[0+i])-1;
    	if(activated[i]==1){
    		if(i!=2&&i!=5)count_offset[i]-=1;
    		if(i==2||i==5)count_offset[i]--;
    		if(count_offset[i]<1) count_offset[i] = 1;
    	}
    	if(motor_signal[BC0+i].at(300-count_offset[0+i])-motor_signal[BC0+i].at(299-count_offset[0+i])<0){
 
        	count_offset[i]--;
        	if(count_offset[i]<0) count_offset[i] = 0;
        	if(i!=2&&i!=5) positions.at(BC0+i) = (motor_signal[BC0+i].at(299-count_offset[0+i])*2)+0.3;
        	if(i==2||i==5) positions.at(BC0+i) = (motor_signal[BC0+i].at(299-count_offset[0+i])*2)+0.8;
        	positions.at(CF0+i) = motor_signal[CF0+i].at(299-count_offset[0+i]);
        	positions.at(FT0+i) = motor_signal[FT0+i].at(299-count_offset[0+i]);
        	if(count_offset[0+i]==0) {st_control[0+i] = 0; activated[i]=0;}
        	count_mem[0+i] = 0;
    	}
    	else if(activated[i]==0) count_offset[0+i]++;
    	if(count_mem[i]+5 == count_offset[i]) { activated[i]=1;}
    }
    /** check for activating signal and assign the legs trajectory ::finished::**/
    cout << setprecision(2) << w_1[i] << "\t" ;
	int index = 499+(int)w_1[i];
	if(index>499) {index=499;}
	if(index<0) {index=0;}
    ico_learning << w_0[i] << " " << w_1[i] << " " << dw_1[i] << " " << reflexive_ico[i].at(1) << " " << reflexive_update[i].at(1) << " " << predictive_ico[i].at(index) << " " << output_ico[i] << " ";
    /****************************Obstacle Avoidance***********finish*************/
    }
    for(int i=0;i<3;i++){
    	ROS_INFO("weight[%d] = %.2f \t weight[%d] = %.2f \t",i,w_1[i],i+3,w_1[i+3]);
    }

    ico_learning << endl;

    sensors_p = sensors;
    if(count_print==150) count_print = 0;
    count_print++;

    std::vector<float> variable_data;
    for(int i=0;i<sensors.size();i++){
    	variable_data.push_back(sensors.at(i));//121
    }
    for(int i=0;i<9;i++){
    	variable_data.push_back(receptor[i]);//122-130
    }
    for(int i=0;i<6;i++){
    	variable_data.push_back(w_1[i]);//131-136
    }
    for(int i=0;i<6;i++){
    	variable_data.push_back(output_ico[i]);//137-142
    }
    for(int i=0;i<6;i++){
        variable_data.push_back(reflexive_update[i].at(1)-reflexive_update[i].at(0));//143-148
    }
    for(int i=0;i<6;i++){
    	int index = 499+(int)w_1[i];
    	if(index>499) {index=499;}
    	if(index<0) {index=0;}
        variable_data.push_back(predictive_ico[i].at(index));//149-154
    }
    realRos->variableValue_Publish(variable_data);
    for(int i=0;i<6;i++){
    	double error = 0;
    	if(sensors.at(FT0_ts+i)>4.0){
    		error = sensors.at(FT0_ts+i) - 4.0;
    	}
    	if(sensors.at(FT0_ts+i)<-4.0){
    	    error = sensors.at(FT0_ts+i) + 4.0;
    	}
    	double old_offset = offset[i];
    	offset[i] += error*0.2;
    	if(error == 0){
    		offset[i] = offset[i]*0.98;
    	}
    	else{
    		offset[i] = offset[i]*0.5 + old_offset*0.5;
    	}
    	positions.at(FT0+i) -= offset[i];
    }
    count_all++;
    ROS_INFO("count_all = %d",count_all-300);
    ROS_INFO("**************************************");

    std::vector<float> positionsNew = {11,-positions.at(BC0),12,positions.at(CF0),13,positions.at(FT0),
                                       21,-positions.at(BC1),22,positions.at(CF1),23,positions.at(FT1),
                                       31,-positions.at(BC2),32,positions.at(CF2),33,positions.at(FT2),
                                       41,positions.at(BC3),42,positions.at(CF3),43,positions.at(FT3),
                                       51,positions.at(BC4),52,positions.at(CF4),53,positions.at(FT4),
                                       61,positions.at(BC5),62,positions.at(CF5),63,positions.at(FT5)};//*/


	std::vector<float> positionsInit =    {11,0.0,12,2.7,13,-1.0,
	                                       21,0.0,22,2.7,23,-1.0,
	                                       31,0.0,32,2.7,33,-1.0,
	                                       41,0.0,42,2.7,43,-1.0,
	                                       51,0.0,52,2.7,53,-1.0,
	                                       61,0.0,62,2.7,63,-1.0};

    if(global_count > 300||(!reflex_active && !global_hormone_active && !distributed_hormone_active)){
        realRos->setLegMotorPosition(positionsNew);
    }
    else{
    	std::vector<float> positionsInit =    {11,0.0,12,2.7,13,-1.0,
    	                                       21,0.0,22,2.7,23,-1.0,
    	                                       31,0.0,32,2.7,33,-1.0,
    	                                       41,0.0,42,2.7,43,-1.0,
    	                                       51,0.0,52,2.7,53,-1.0,
    	                                       61,0.0,62,2.7,63,-1.0};
    	realRos->setLegMotorPosition(positionsInit);
    }
}
