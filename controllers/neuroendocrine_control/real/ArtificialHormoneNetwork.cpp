#include "ArtificialHormoneNetwork.h"


//for cout
//#include <selforg/controller_misc.h>

//using namespace matrix;

using namespace Hormone;

using namespace std;

ArtificialHormoneNetwork::ArtificialHormoneNetwork() {

  outFileAHN.open("ArtificialHormoneNetwork.txt");
  outFileBB.open("DATA-BB.txt");
  for(int i=0;i<18;i++){
      outFileBB << "y[" << i << "] ";
  }
  for(int i=0;i<36;i++){
      outFileBB << "sensors[" << i << "] ";
  }
  for(int i=0;i<18;i++){
      outFileBB << "mpre[" << i << "] ";
  }
  for(int i=0;i<6;i++){
      outFileBB << "m_pre_use[" << i << "] ";
  }
  for(int i=0;i<6;i++){
      outFileBB << "torque[" << i << "] ";
  }
  for(int i=0;i<6;i++){
      outFileBB << "error[" << i << "] ";
  }
  outFileBB  << endl;
  for(int i=0;i<9;i++){
      outFileAHN << "H[" << i << "] ";
  }
  for(int i=0;i<6;i++){
      outFileAHN << "Lift[" << i << "] ";
  }
  for(int i=0;i<7;i++){
      outFileAHN << "error[" << i << "] ";
  }
  for(int i=0;i<6;i++){
	  outFileAHN << "Acc_error[" << i << "] ";
  }
  outFileAHN << "Or_y ";
  outFileAHN  << endl;

// BB-THESIS --> This is my manual document. following this tag "BB-THESIS STEP-n" (n is step sequence) read the instruction and follow it. Enjoy :)
//initial data to defaut
  //parameter setting
  windows_size = 300; //B-can edit

  //parameter Hormonemotormap.at(i)->max_ctr = 130
  gland_size = 9;		//change number of Gland ,Hormone ,Receptor ,Internal all you want to control your robot BB-THESIS STEP-1
  hormone_size = 9;		//
  receptor_size = 9;	//
  internal_size = 36;	//

  clearData(in0, 122, windows_size);
  clearData(out0, 19, windows_size);
  clearData(inside0, internal_size, windows_size);

  clearData(data_sensor, 122, windows_size);
  clearData(data_motor, 19, windows_size);
  clearData(data_inside, internal_size * 2, windows_size);

}

ArtificialHormoneNetwork::~ArtificialHormoneNetwork() {
  outFileAHN.close();
  outFileBB.close();
}

void ArtificialHormoneNetwork::initialAHN() {
  createAHN();
  hormoneConnection();
  hormoneConfig();

}

void ArtificialHormoneNetwork::createAHN() {
  createGland(gland_size);
  createHormoneTank(hormone_size);
  createReceptor(receptor_size);
}

void ArtificialHormoneNetwork::hormoneConfig() {
  //owner design
  configGlandAlpha(0, 0.3); //B-can edit
  configGlandType(0, HM_Mean, HM_RECORD_FIFO);//B-can edit
  // configGlandType(0, HM_SD, HM_RECORD_FIFO);//B-can edit
  configGlandBufferSize(0, 12); //B-can edit
  configTankBeta(0, 0.0205); //B-can edit
  //configGlandMode(0,0,3);
  configReceptorRate(0, 0, 0.0005);//B-can edit link to 258 line

  configGlandAlpha(1, 0.3); //B-can edit
  configGlandType(1, HM_Mean, HM_RECORD_FIFO);//B-can edit
  // configGlandType(0, HM_SD, HM_RECORD_FIFO);//B-can edit
  configGlandBufferSize(1, 12); //B-can edit
  configTankBeta(1, 0.0205); //B-can edit
  //configGlandMode(0,0,3);
  configReceptorRate(1, 1, 0.0005);//B-can edit link to 258 line

  configGlandAlpha(2, 0.3); //B-can edit
  configGlandType(2, HM_Mean, HM_RECORD_FIFO);//B-can edit
  // configGlandType(0, HM_SD, HM_RECORD_FIFO);//B-can edit
  configGlandBufferSize(2, 12); //B-can edit
  configTankBeta(2, 0.0205); //B-can edit
  //configGlandMode(0,0,3);
  configReceptorRate(2, 2, 0.0005);//B-can edit link to 258 line

  configGlandAlpha(3, 0.3); //B-can edit
  configGlandType(3, HM_Mean, HM_RECORD_FIFO);//B-can edit
  // configGlandType(0, HM_SD, HM_RECORD_FIFO);//B-can edit
  configGlandBufferSize(3, 12); //B-can edit
  configTankBeta(3, 0.0205); //B-can edit
  //configGlandMode(0,0,3);
  configReceptorRate(3, 3, 0.0005);//B-can edit link to 258 line

  configGlandAlpha(4, 0.3); //B-can edit
  configGlandType(4, HM_Mean, HM_RECORD_FIFO);//B-can edit
  // configGlandType(0, HM_SD, HM_RECORD_FIFO);//B-can edit
  configGlandBufferSize(4, 12); //B-can edit
  configTankBeta(4, 0.0205); //B-can edit
  //configGlandMode(0,0,3);
  configReceptorRate(4, 4, 0.0005);//B-can edit link to 258 line

  configGlandAlpha(5, 0.3); //B-can edit
  configGlandType(5, HM_Mean, HM_RECORD_FIFO);//B-can edit
  // configGlandType(0, HM_SD, HM_RECORD_FIFO);//B-can edit
  configGlandBufferSize(5, 12); //B-can edit
  configTankBeta(5, 0.0205); //B-can edit
  //configGlandMode(0,0,3);
  configReceptorRate(5, 5, 0.0005);//B-can edit link to 258 line

  configGlandAlpha(6, 0.3); //B-can edit
  configGlandType(6, HM_Mean, HM_RECORD_FIFO);//B-can edit
  // configGlandType(0, HM_SD, HM_RECORD_FIFO);//B-can edit
  configGlandBufferSize(6, 12); //B-can edit
  configTankBeta(6, 0.0205); //B-can edit
  //configGlandMode(0,0,3);
  configReceptorRate(6, 6, 0.0005);//B-can edit link to 258 line

  configGlandAlpha(7, 0.3); //B-can edit
  configGlandType(7, HM_Mean, HM_RECORD_FIFO);//B-can edit
  // configGlandType(0, HM_SD, HM_RECORD_FIFO);//B-can edit
  configGlandBufferSize(7, 12); //B-can edit
  configTankBeta(7, 0.0205); //B-can edit
  //configGlandMode(0,0,3);
  configReceptorRate(7, 7, 0.0005);//B-can edit link to 258 line

  configGlandAlpha(8, 0.3); //B-can edit
  configGlandType(8, HM_Mean, HM_RECORD_FIFO);//B-can edit
  // configGlandType(0, HM_SD, HM_RECORD_FIFO);//B-can edit
  configGlandBufferSize(8, 12); //B-can edit
  configTankBeta(8, 0.0205); //B-can edit
  //configGlandMode(0,0,3);
  configReceptorRate(8, 8, 0.0005);//B-can edit link to 258 line

}

void ArtificialHormoneNetwork::hormoneConnection() {
	// owner design
  connectGlandToTank(0, 0);
  //connectTankToGland(0, 0);
  connectReceptorToTank(0, 0);
  //connectReceptorToTank(0, 1);

  connectGlandToTank(1, 1);
  //connectTankToGland(0, 0);
  connectReceptorToTank(1, 1);
  //connectReceptorToTank(0, 1);

  connectGlandToTank(2, 2);
  //connectTankToGland(0, 0);
  connectReceptorToTank(2, 2);
  //connectReceptorToTank(0, 1);

  connectGlandToTank(3, 3);
  //connectTankToGland(0, 0);
  connectReceptorToTank(3, 3);
  //connectReceptorToTank(0, 1);

  connectGlandToTank(4, 4);
  //connectTankToGland(0, 0);
  connectReceptorToTank(4, 4);
  //connectReceptorToTank(0, 1);

  connectGlandToTank(5, 5);
  //connectTankToGland(0, 0);
  connectReceptorToTank(5, 5);
  //connectReceptorToTank(0, 1);type filter text

  connectGlandToTank(6, 6);
  //connectTankToGland(0, 0);
  connectReceptorToTank(6, 6);
  //connectReceptorToTank(0, 1);

  connectGlandToTank(7, 7);
  //connectTankToGland(0, 0);
  connectReceptorToTank(7, 7);
  //connectReceptorToTank(0, 1);

  connectGlandToTank(8, 8);
  //connectTankToGland(0, 0);
  connectReceptorToTank(8, 8);
  //connectReceptorToTank(0, 1);

}

void ArtificialHormoneNetwork::configGlandAlpha(unsigned int _Address, float _Alpha) {
  if (_Address < HGland.size()) {
    if (_Alpha > 1) {
      _Alpha = 1;
    }
    HGland[_Address].setALPHA(_Alpha);

  }
}

void ArtificialHormoneNetwork::configGlandBufferSize(unsigned int _Address, unsigned int _Buffersize) {
  if (_Address < HGland.size()) {
    HGland[_Address].data.setBufferSize(_Buffersize);
  }
}

void ArtificialHormoneNetwork::configGlandType(unsigned int _Address, int _Type, int _Recordtype) {
  if (_Address < HGland.size()) {
    HGland[_Address].data.setInputType(_Type, _Recordtype);
  }
}

void ArtificialHormoneNetwork::configGlandParam(unsigned int _Address, unsigned int _Destination, DATATYPE) {

}

void ArtificialHormoneNetwork::configGlandWeight(unsigned int _Address, unsigned int _Destination, DATATYPE) {

}


void ArtificialHormoneNetwork::configTankBeta(unsigned int _Address, float _Beta) {
  if (_Address < HTank.size()) {
    if (_Beta > 1) {
      _Beta = 1;
    }
    HTank[_Address].setBETA(_Beta);
  }
}

void ArtificialHormoneNetwork::configReceptorRate(unsigned int _Address, unsigned int _Destination, DATATYPE rate) {
  if (_Address < HReceptor.size() && _Destination < HTank.size()) {
    if (rate > 1.0) {
      rate = 1.0;
    } else if (rate < 0) {
      rate = 0.0;
    }
    HReceptor[_Address].setReceptorRate(&HTank[_Destination], rate);
  }
}

void ArtificialHormoneNetwork::configReceptorWeight(unsigned int _Address, unsigned int _Destination, DATATYPE weight) {
  if (_Address < HReceptor.size() && _Destination < HTank.size()) {
    if (weight > 1.0) {
      weight = 1.0;
    } else if (weight < 0) {
      weight = 0.0;
    }
    HReceptor[_Address].setReceptorWeight(&HTank[_Destination], weight);
  }
}

void ArtificialHormoneNetwork::configGlandMode(unsigned int _Address, unsigned int _Destination, int mode) {
  if (_Address < HGland.size() && _Destination < HTank.size()) {
    if (mode < 1 && mode > 4) {
      mode = 1;
    }
    HGland[_Address].setFeedBackMode(&HTank[_Destination], mode);
  }
}

void ArtificialHormoneNetwork::connectGlandToTank(unsigned int _Address, unsigned int _Destination) {
  HGland[_Address].LinkToHormone(&HTank[_Destination]);
}

void ArtificialHormoneNetwork::connectTankToGland(unsigned int _Address, unsigned int _Destination, int mode,
    DATATYPE parameter, DATATYPE weight) {
  HGland[_Destination].setFeedBack(&HTank[_Address], mode, parameter, weight);
}

void ArtificialHormoneNetwork::connectReceptorToTank(unsigned int _Address, unsigned int _Destination, DATATYPE rate,
    DATATYPE Weight) {
  HReceptor[_Address].AddHormoneReceptor(&HTank[_Destination], rate, Weight);
}

void ArtificialHormoneNetwork::externalStimulus(const vector<double> x, const vector<double> y) {

  if (x.size() >= y.size()) {
    for (unsigned int i = 0; i < x.size(); i++) {
      in0[i].push_back(x.at(i));
      if (in0[i].size() > windows_size) {
        in0[i].erase(in0[i].begin());
      }
      if (i < y.size()) {
        out0[i].push_back(y.at(i));
        if (out0[i].size() > windows_size) {
          out0[i].erase(out0[i].begin());
        }
      }
    }
  } else {
    for (unsigned int i = 0; i < y.size(); i++) {
      out0[i].push_back(y.at(i));
      if (out0[i].size() > windows_size) {
        out0[i].erase(out0[i].begin());
      }
      if (i < x.size()) {
        in0[i].push_back(x.at(i));
        if (in0[i].size() > windows_size) {
          in0[i].erase(in0[i].begin());
        }
      }
    }
  }
  for(int i=0;i<18;i++){
    
    outFileBB << out0[i].at(windows_size-1) << " " ;
  }
  //std::cout << endl;
  for(int i=0;i<36;i++){
    //std::cout << x.at(i) << " " ;
    outFileBB << in0[i].at(windows_size-1) << " " ;
  }
}

void ArtificialHormoneNetwork::internalStimulus(const vector<double> x) {
  //std::cout << inside0.size() << endl;
  if (x.size() <= inside0.size()) {
    for (unsigned int i = 0; i < x.size(); i++) {
      inside0[i].push_back(x.at(i));
      if (inside0[i].size() > windows_size) {
        inside0[i].erase(inside0[i].begin());
      }
    }
  } else {
    std::cout << "size not match" << endl;
    clearData(inside0, inside0.size(), windows_size);
    for (unsigned int i = 0; i < x.size(); i++) {
      inside0[i].push_back(x.at(i));
      if (inside0[i].size() > windows_size) {
        inside0[i].erase(inside0[i].begin());
      }
    }
  }
  for(int i=0;i<18;i++){
    outFileBB << inside0[i].at(windows_size-1) << " " ;
  }
}

void ArtificialHormoneNetwork::hormoneProcess() {
  preprocess();
  updateDataGland();
  updateHormone();
}

void ArtificialHormoneNetwork::preprocess() {
  // prepare data before into hormone-framework]
  //dirct data
  data_sensor = in0;
  data_motor = out0;
  //data_inside = inside0;

  //modify data
  auto it = data_inside.begin();
  data_inside.insert(it + internal_size, inside0.begin(), inside0.end());
  data_inside.resize(internal_size * 2);
  double sc[6] = {0.15,0.15,0.15,0.15,0.15,0.15};
  birdModel(0.5, sc);   //B-can edit
  //gearModel(1.3,0.25);

}

void ArtificialHormoneNetwork::updateDataGland() {
  // data management and add to gland
  for (unsigned int i = 0; i < gland_size; i++) {
	if(inside0[6+i].at(windows_size - 1)<=-0.964 || i > 5)
	{
		if(i<6){
			if(inside0[30+i].at(windows_size -1) == 0)
				HGland[i].data.Add(data_inside[i + internal_size + 6].at(windows_size - 1));
		}
		else{
			HGland[i].data.Add(data_inside[i + internal_size + 6].at(windows_size - 1));
		}
	}
  }
}

void ArtificialHormoneNetwork::updateHormone() {
  //all hormone-framework
  for (unsigned int i = 0; i < gland_size; i++) {

		if(inside0[6+i].at(windows_size - 1)<=-0.964 || i > 5)
		{
			if(i<6){
				if(inside0[30+i].at(windows_size -1) == 0)
					HGland[i].produceHormone();
			}
			else{
				HGland[i].produceHormone();
			}
		}
  }

  for (unsigned int i = 0; i < hormone_size; i++) {
		if(inside0[6+i].at(windows_size - 1)<=-0.964 || i > 5)
		{
			if(i<6){
				if(inside0[30+i].at(windows_size -1) == 0)
					HTank[i].Active(i);
			}
			else{
				HTank[i].Active(i);
			}
		}
  }
}

DATATYPE ArtificialHormoneNetwork::receptorActive(unsigned int _Address) {
  DATATYPE expression;
  expression = HReceptor[_Address].reduceHormone();
  switch (_Address) {
    case 0:
      expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.0, 1.0); //B-can edit
      // expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.998, 1.002); //B-can edit
      // std::cout << "receptoravtice = " << expression <<endl;
      break;
    case 1:
      expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.0, 1.0); //B-can edit
      // expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.998, 1.002); //B-can edit
      // std::cout << "receptoravtice = " << expression <<endl;
      break;
    case 2:
      expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.0, 1.0); //B-can edit
      // expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.998, 1.002); //B-can edit
      // std::cout << "receptoravtice = " << expression <<endl;
      break;
    case 3:
      expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.0, 1.0); //B-can edit
      // expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.998, 1.002); //B-can edit
      // std::cout << "receptoravtice = " << expression <<endl;
      break;
    case 4:
      expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.0, 1.0); //B-can edit
      // expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.998, 1.002); //B-can edit
      // std::cout << "receptoravtice = " << expression <<endl;
      break;
    case 5:
      expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.0, 1.0); //B-can edit
      // expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.998, 1.002); //B-can edit
      // std::cout << "receptoravtice = " << expression <<endl;
      break;

     case 6:
      expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.0, 1.0); //B-can edit
      // expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.998, 1.002); //B-can edit
      // std::cout << "receptoravtice = " << expression <<endl;
      break;

     case 7:
      expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 1.0, 2.0); //B-can edit
      // expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.998, 1.002); //B-can edit
      // std::cout << "receptoravtice = " << expression <<endl;
      break;

     case 8:
      expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 1.0, 2.0); //B-can edit
      // expression = RangeCast<DATATYPE>(expression, 0, 0.0005, 0.998, 1.002); //B-can edit
      // std::cout << "receptoravtice = " << expression <<endl;
      break;
  }
  return expression;
}

DATATYPE ArtificialHormoneNetwork::getHormoneConcentration(unsigned int _Address) {
  DATATYPE concentration;
  concentration = HTank[_Address].hormoneValue;

  return concentration;
}

DATATYPE ArtificialHormoneNetwork::getDataInside(unsigned int _Address){
  return data_inside[_Address].at(windows_size-1);
}

void ArtificialHormoneNetwork::createGland(unsigned int _size) {
  HGland.assign(_size, gland());
}
void ArtificialHormoneNetwork::createHormoneTank(unsigned int _size) {
  HTank.assign(_size, hormone());
}
void ArtificialHormoneNetwork::createReceptor(unsigned int _size) {
  HReceptor.assign(_size, receptor());
}


void ArtificialHormoneNetwork::reAHN() {
}

void ArtificialHormoneNetwork::setWindowsSize(unsigned int unsignedInt) {
}

unsigned int ArtificialHormoneNetwork::getWindowsSize() {
}

void ArtificialHormoneNetwork::setInternalSize(unsigned int _size) {
  internal_size = _size;
  clearData(inside0, internal_size, windows_size);
}

unsigned int ArtificialHormoneNetwork::getInternalSize() {
  return internal_size;
}

void ArtificialHormoneNetwork::clearData(vector<double> &x, unsigned int _size, DATATYPE _data) {
  x.resize(_size);
  for (unsigned int i = 0; i < x.size(); i++) {
    x.push_back(_data);
    if (x.size() > windows_size) {
      x.erase(x.begin());
    }
  }
}

void ArtificialHormoneNetwork::clearData(vector<vector<double>> &x, unsigned int _size, unsigned int _windows,
    DATATYPE _data) {
  x.resize(_size);
  for (unsigned int i = 0; i < x.size(); i++) {
    x[i].resize(_windows);
    for (unsigned int j = 0; j < x[i].size(); j++) {
      x[i].push_back(_data);
      if (x[i].size() > _windows) {
        x[i].erase(x[i].begin());
      }
    }
  }
}

void ArtificialHormoneNetwork::setGlandSize(unsigned int unsignedInt) {
}

unsigned int ArtificialHormoneNetwork::getGlandSize() {
}


void ArtificialHormoneNetwork::setReceptorSize(unsigned int unsignedInt) {
}

unsigned int ArtificialHormoneNetwork::getReceptorSize() {
}


void ArtificialHormoneNetwork::setHormoneSize(unsigned int unsignedInt) {
}

unsigned int ArtificialHormoneNetwork::getHormoneSize() {
  return hormone_size;
}
double findScaling(double value){
	return 1/(7*sin(value*0.001)) ;
}
void ArtificialHormoneNetwork::birdModel(double weight, double scaling[6]) {
  DATATYPE _data[6];
  //if (inside0.size() * 2 <= data_inside.size()) {
    for (unsigned int i = 0; i < 6; i++) {
      //low pass filter
      double m_pre = (((-inside0[6+i].at(windows_size - 1) + 1)/2) * weight + (data_inside[6+i].at(windows_size - 1)*(1/scaling[i])) * (1-weight)) ;
      data_inside[6 + i].push_back(m_pre*scaling[i]);
      if (data_inside[6 + i].size() > windows_size) {//6 to 11
        data_inside[6 + i].erase(data_inside[6 + i].begin());
      }

      //assign the hormone stimuli _data[i]
      _data[i]  = ((data_inside[6 + i].at(windows_size -1)*2.0* findScaling(inside0[18+i].at(windows_size-1)))  - ((in0[24+i].at(windows_size - 1)))); //B-can edit
      if(inside0[6+i].at(windows_size - 1)>=-0.964 || _data[i] < 0.0) {_data[i]=0;} //_data = 0 when the leg is swinging

      data_inside[6 + i + internal_size].push_back(_data[i]); //B-can edit
      if (data_inside[6 + i + internal_size].size() > windows_size) {
        data_inside[6 + i + internal_size].erase(data_inside[6 + i + internal_size].begin());
      }
    }

     double _dataa = (_data[0]+_data[1]+_data[2]+_data[3]+_data[4]+_data[5])/6.0;
     data_inside[6 + 6 + internal_size].push_back(_dataa);
     if (data_inside[6 + 6 + internal_size].size() > windows_size) {
        data_inside[6 + 6 + internal_size].erase(data_inside[6 + 6 + internal_size].begin());
      }

     double data_imu[2] = {-in0[36].at(windows_size-1),in0[36].at(windows_size-1)};
     for(int i=0;i<2;i++)
    	 if(data_imu[i]<0.0001)
    		 data_imu[i]=0;
     data_inside[6 + 7 + internal_size].push_back(data_imu[0]);
     if (data_inside[6 + 7 + internal_size].size() > windows_size) {
        data_inside[6 + 7 + internal_size].erase(data_inside[6 + 7 + internal_size].begin());
      }
     data_inside[6 + 8 + internal_size].push_back(data_imu[1]);
     if (data_inside[6 + 8 + internal_size].size() > windows_size) {
        data_inside[6 + 8 + internal_size].erase(data_inside[6 + 8 + internal_size].begin());
      }
    for(int i=0;i<6;i++){
        outFileBB << (data_inside[6 + i].at(windows_size -1)*2.0* findScaling(inside0[18+i].at(windows_size-1))) << " " ;
    }
    for(int i=0;i<6;i++){
        outFileBB << in0[24+i].at(windows_size - 1) << " " ;
    }
    for(int i=0;i<6;i++){
        outFileBB << _data[i] << " " ;
    }
    outFileBB << endl;
    for(int i=0;i<9;i++){//collect useful data for hormone
        outFileAHN << getHormoneConcentration(i) <<" ";
    }
    for(int i=0;i<6;i++){
        outFileAHN << inside0[18+i].at(windows_size-1) << " ";
    }
    for(int i=0;i<6;i++){
        outFileAHN << _data[i] <<  " ";
    }
        outFileAHN << _dataa <<  " ";
    for(int i=0;i<6;i++){
        outFileAHN << inside0[24+i].at(windows_size-1) <<  " ";
    }
    outFileAHN << in0[37].at(windows_size-1) << " ";
    outFileAHN  << endl;

}

DATATYPE ArtificialHormoneNetwork::Convolution(const vector<double> x, const vector<double> y) {
  std::vector<long double> conv;
  double norm = 0;
  DATATYPE out_conv;

  conv.resize(x.size());

  //Convolution
  for (unsigned int i = 0; i < x.size(); i++) {
    conv.at(i) = 0; // set to zero before sum
    for (unsigned int j = 0; j < y.size(); j++) {
      if (i - j >= 0) {
        conv.at(i) += x[i - j] * y[j]; // convolve: multiply and accumulate
      }
    }
    //Normalize
    if (conv.at(i) != 0) {
      norm += pow(conv.at(i), 2);
    }
  }

  double max_conv = *std::max_element(std::begin(conv), std::end(conv));

  if (norm != 0) {
    out_conv = max_conv / sqrt(norm);
  } else {
    out_conv = 0;
  }

  return out_conv;

}

DATATYPE ArtificialHormoneNetwork::Correlation(const vector<double> x, const vector<double> y) {
  DATATYPE correlation;
  DATATYPE cov_xy = calcCov(x, y);
  DATATYPE sd_x = calcSD(x);
  DATATYPE sd_y = calcSD(y);

  if (cov_xy == 0) {
    correlation = 0;
  } else {
    correlation = cov_xy / sqrt(sd_x * sd_y);
  }

  if (correlation < 0.0) {
    correlation = -correlation;
  }

  return correlation;
}

DATATYPE ArtificialHormoneNetwork::calcSD(vector<double> inputVector) {
  double Xbar = calcMean(inputVector);
  double sum = 0;
  for (unsigned int i = 0; i < inputVector.size(); i++) {
    sum += pow(inputVector[i] - Xbar, 2);
  }
  sum /= inputVector.size();
  return (DATATYPE) sum;
}
DATATYPE ArtificialHormoneNetwork::calcMean(vector<double> inputVector) {
  DATATYPE sum = 0;
  for (unsigned int i = 0; i < inputVector.size(); i++) {
    sum += inputVector[i];
  }
  sum /= inputVector.size();
  return sum;
}

DATATYPE ArtificialHormoneNetwork::calcCov(const vector<double> x, const vector<double> y) {

  DATATYPE Xbar = calcMean(x);
  DATATYPE Ybar = calcMean(y);

  DATATYPE sum = 0;

  for (unsigned int i = 0; i < x.size(); i++) {
    sum += (x[i] - Xbar) * (y[i] - Ybar);
  }

  return sum / x.size();
}
