//
// Created by mat on 12/30/17.
//

#ifndef NEURAL_CONTROL_H
#define NEURAL_CONTROL_H

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <algorithm>
#include <ros/ros.h>
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"
#include <std_msgs/Int32.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/Float32MultiArray.h"
#include "realRosClass.h"
#include "neutronMotorDefinition.h"
#include "ArtificialHormoneNetwork.h"

class NeuralControl {
public:
	NeuralControl();
    ~NeuralControl();
    void step();
    std::vector<double> m_pre;
    std::vector<double> Control_input;
private:
    std::vector<double> buffer_motor[3];
    std::vector<double> count_up;
    std::vector<double> count_down;
    std::vector<double> count_upold;
    std::vector<double> count_downold;
    std::vector<double> x_up;
    std::vector<double> x_down;
    std::vector<double> y_up;
    std::vector<double> y_down;
    std::vector<double> set;
    std::vector<double> delta_xup;
    std::vector<double> delta_xdown;
    std::vector<double> pcpg_output;
    std::vector<double> diff_set;
    double back=1.0;
    int global_count=0;
    int inner_count[6]={0,0,0,0,0,0};
    std::vector<double> acc_error;
    std::vector<std::vector<double>> cpg_w;
    std::vector<double> cpg_activity;
    std::vector<double> cpg_output;
    double cpg_bias;


    int tau=250;       // Tripod 18/45
    int tauLeft=250;   // Wave
    int count=0;
};


#endif //NEUTRON_CONTROLLER_NEUTRONCONTROLLER_H
